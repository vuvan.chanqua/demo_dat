# Bokeh streaming server

### Install dependencies
Cài đặt thư viện cho project

```
$ pip install -r requirements.txt
```

### Config ip 
Cấu hình địa chỉ ip cho client trong file /bokeh_receiver/receiver.py 

```
77: zmq_socket.connect("tcp://192.168.31.251:9000")
```


### Run
Để chạy server chạy file run.sh
```
# Server. Run
./run.sh
```
Truy cập `http://localhost:5006/bokeh_receiver` trong trình duyệt để theo dõi
